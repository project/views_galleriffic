Instructions:


1) Download Galleriffic plugin from http://www.twospy.com/galleriffic/

2) place 'js' folder with files inside 'views_galleriffic' module folder. Should be 'views_galleriffic/js'

3) Make a node with one or more imagefield fields.  Make two imagecache presets, one for the thumbnail and one for the main slide image.

4) Make a view using that node type.

5) Select 'Galleriffic' style. You can add your own style settings or use the defaults.

6) Select 'Galleriffic' for the row style. You will select a Title, description, and two imagefields. THE IMAGEFIELDS MUST USE AN IMAGECACHE PRESET in the field format. You can use the same image twice with different imagecache presets.  This is my preferred functionality since users only have to upload one image for slide. Though some users may want to have a thumbnail that is different from the slide.

7) You should be done. You can make a page or attach the gallery to a node. The imagefield can accept multiple arguments so you could use this as a gallery of images uploaded to a single node.


TO-DO's

+ Add default css. Out of the box this doesn't look so hot.

+ Find a better way, hopefully using views, to get filepath.  Current way is not elegent and forces the reliance on imagecache.

+ Setup better warnings for those who do not turn on imagecache and use imagefield.