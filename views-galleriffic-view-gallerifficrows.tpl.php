<?php
// $Id: 
?>
<li>
  <a class="thumb" href="<?php print $fields['slide_field']->raw; ?>" title="<?php print htmlspecialchars($fields['title_field']->raw); ?>">
    <img src="<?php print $fields['thumbnail_field']->raw; ?>" alt="<?php print htmlspecialchars($fields['title_field']->raw); ?>" />
  </a>
  <div class="caption">
  <?php if($fields['slide_field']->filepath): ?>
		<div class="download">
			<a href="/<?php print $fields['slide_field']->filepath; ?>">Download Original</a>
		</div>
  <?php endif; ?>
		<div class="image-title"><?php print $fields['title_field']->content; ?></div>
		<div class="image-desc"><?php print $fields['description_field']->content; ?></div>
	</div>
</li>

