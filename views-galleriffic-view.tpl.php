<?php
// $Id: 
/**
 * @file
 *  Views Galleriffic theme wrapper.
 *
 * @ingroup views_templates
 */
drupal_add_js(drupal_get_path('module', 'views_galleriffic') .'/js/jush.js');
drupal_add_js(drupal_get_path('module', 'views_galleriffic') .'/js/jquery.galleriffic.js');
drupal_add_js(drupal_get_path('module', 'views_galleriffic') .'/js/views_galleriffic.js');
?>
<div id="gallery" class="content">
  <div id="controls" class="controls"></div>
  <div id="loading" class="loader"></div>
  <div id="slideshow" class="slideshow"></div>
  <div id="caption" class="embox"></div>
</div>
<div id="thumbs" class="navigation">
  <ul class="thumbs noscript">
    <?php foreach ($rows as $row): ?>
      <?php print $row?>
    <?php endforeach; ?>
  </ul>
</div> <!-- end gallery -->

<script type="text/javascript">

  // Initially set opacity on thumbs and add
  // additional styling for hover effect on thumbs
  var onMouseOutOpacity = 0.67;
  $('#thumbs ul.thumbs li').css('opacity', onMouseOutOpacity)
   .hover(
     function () {
       $(this).not('.selected').fadeTo('fast', 1.0);
     },
     function () {
      $(this).not('.selected').fadeTo('fast', onMouseOutOpacity);
     }
   );

			$(document).ready(function() {
				// Initialize Advanced Galleriffic Gallery
				var galleryAdv = $('#gallery').galleriffic('#thumbs', {
					delay:                  <?php print $option['delay'];?>,
					numThumbs:              <?php print $numbthumbs;?>,
					preloadAhead:           10,
					enableTopPager:         <?php print $option['pager_top'];?>,
					enableBottomPager:      <?php print $option['pager_bottom'];?>,
					imageContainerSel:      '#slideshow',
					controlsContainerSel:   '#controls',
					captionContainerSel:    '#caption',
					loadingContainerSel:    '#loading',
					renderSSControls:       <?php print $option['renderss'];?>,
					renderNavControls:      <?php print $option['rendernav'];?>,
					playLinkText:           '<?php print $option['playtext'];?>',
					pauseLinkText:          '<?php print $option['pausetext'];?>',
					prevLinkText:           '<?php print $option['prevlink'];?>',
					nextLinkText:           '<?php print $option['nextlink'];?>',
					nextPageLinkText:       '<?php print $option['nextpage'];?>',
					prevPageLinkText:       '<?php print $option['prevpage'];?>',
					enableHistory:          <?php print $option['history'];?>,
					autoStart:              <?php print $option['start'];?>,
					onChange:               <?php print $option['change'];?>,
					onTransitionOut:        <?php print $option['transout'];?>,
					onTransitionIn:         <?php print $option['transin'];?>,
					onPageTransitionOut:    <?php print $option['pageout'];?>,
					onPageTransitionIn:     <?php print $option['pagein'];?>,
				});
			});
		</script>
