<?php
// $Id: 
/**
 * @file
 *  Provide the views gallariffic plugin object with default options and form.
 */

/**
  * Implementation of views_plugin_style().
  */
class views_galleriffic_style_plugin extends views_plugin_style {

  function option_definition() {
    $options = parent::option_definition();
    unset($options['grouping']);
    $options['delay']     = array('default'=> '3000');
    $options['pager_top']   = array('default'=> 'false');
    $options['pager_bottom']      = array('default'=> 'true');
    $options['renderss']     = array('default'=> 'true');
    $options['rendernav'] = array('default'=> 'true');
    $options['playtext']    = array('default'=> 'Play');
    $options['pausetext']    = array('default'=> 'Pause');
    $options['prevlink']    = array('default'=> 'Previous');
    $options['nextlink']    = array('default'=> 'Next');	
    $options['nextpage']    = array('default'=> 'Next &rsaquo;');	
    $options['prevpage']    = array('default'=> '&lsaquo; Prev');	
    $options['history']    = array('default'=> 'false');	
    $options['start']    = array('default'=> 'false');	
    $options['change']    = array('default'=> 'function(prevIndex, nextIndex) {	$(\'#thumbs ul.thumbs\').children()	.eq(prevIndex).fadeTo(\'fast\', onMouseOutOpacity).end() .eq(nextIndex).fadeTo(\'fast\', 1.0);}');
    $options['transout']    = array('default'=> 'function(callback) {$(\'#caption\').fadeOut(\'fast\');	$(\'#slideshow\').fadeOut(\'fast\', callback);}');	
    $options['transin']    = array('default'=> 'function() {$(\'#slideshow, #caption\').fadeIn(\'fast\');}');		
    $options['pageout']    = array('default'=> 'function(callback) {$(\'#thumbs ul.thumbs\').fadeOut(\'fast\', callback);}');
    $options['pagein']    = array('default'=> 'function() {$(\'#thumbs ul.thumbs\').fadeIn(\'fast\');}');		
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    unset($form['grouping']);

    $form['delay'] = array(
      '#type' => 'textfield',
      '#title' => t('Delay'),
      '#default_value' => $this->options['delay'],
      '#description' => t('The delay between each slide. In miliseconds.'),
    );
    $form['pager_top'] = array(
      '#type' => 'select',
      '#title' => t('Enable Top Pager'),
      '#description' => t('Enable the pager at the top of the gallery.'),
      '#default_value' => $this->options['pager_top'],
      '#options' => array('true'=>t('Yes'), 'false' => t('No') ),
    );
    $form['pager_bottom'] = array(
      '#type' => 'select',
      '#title' => t('Enable Bottom Pager'),
      '#description' => t('Enable the pager at the bottom of the gallery.'),
      '#default_value' => $this->options['pager_bottom'],
      '#options' => array('true'=>t('Yes'), 'false' => t('No') ),
    );	
    $form['renderss'] = array(
      '#type' => 'select',
      '#title' => t('Show Play and Pause Controls'),
      '#description' => t('Specifies whether the slideshow\'s Play and Pause links should be rendered.'),
      '#default_value' => $this->options['renderss'],
      '#options' => array('true'=>t('Yes'), 'false' => t('No') ),
    );
    $form['rendernav'] = array(
      '#type' => 'select',
      '#title' => t('Show Next and Pause Controls'),
      '#description' => t('Specifies whether the slideshow\'s Next and Previous links should be rendered.'),
      '#default_value' => $this->options['rendernav'],
      '#options' => array('true'=>t('Yes'), 'false' => t('No') ),
    );
    $form['playtext'] = array(
      '#type' => 'textfield',
      '#title' => t('Play Link Text'),
      '#default_value' => $this->options['playtext'],
      '#description' => t('Text to display for the PLAY link.'),
    );
    $form['pausetext'] = array(
      '#type' => 'textfield',
      '#title' => t('Pause Link Text'),
      '#default_value' => $this->options['pausetext'],
      '#description' => t('Text to display for the PAUSE link.'),
    );
    $form['prevlink'] = array(
      '#type' => 'textfield',
      '#title' => t('Previous Link Text'),
      '#default_value' => $this->options['prevlink'],
      '#description' => t('Text to display for the PREVIOUS link.'),
    );
    $form['nextlink'] = array(
      '#type' => 'textfield',
      '#title' => t('Next Link Text'),
      '#default_value' => $this->options['nextlink'],
      '#description' => t('Text to display for the NEXT link.'),
    );
    $form['nextpage'] = array(
      '#type' => 'textfield',
      '#title' => t('Next Page Link Text'),
      '#default_value' => $this->options['nextpage'],
      '#description' => t('Text to display for the next PAGE link.'),
    );
    $form['prevpage'] = array(
      '#type' => 'textfield',
      '#title' => t('Previous Page Link Text'),
      '#default_value' => $this->options['prevpage'],
      '#description' => t('Text to display for the next PREVIOUS link.'),
    );
    $form['history'] = array(
      '#type' => 'select',
      '#title' => t('Enable History'),
      '#description' => t('Specifies whether the url\'s hash and the browser\'s history cache should update when the current slideshow image changes.'),
      '#default_value' => $this->options['history'],
      '#options' => array('true'=>t('Yes'), 'false' => t('No') ),
    );
    $form['start'] = array(
      '#type' => 'select',
      '#title' => t('Auto Start'),
      '#description' => t('Specifies whether the slideshow should be playing or paused when the page first loads.'),
      '#default_value' => $this->options['start'],
      '#options' => array('true'=>t('Yes'), 'false' => t('No') ),
    );
    $form['change'] = array(
      '#type' => 'textarea',
      '#title' => t('On Change Function'),
      '#description' => t('accepts a delegate like such: function(prevIndex, nextIndex) { ... }.'),
      '#default_value' => $this->options['change'],
    );
    $form['transout'] = array(
      '#type' => 'textarea',
      '#title' => t('Transition Out Function'),
      '#description' => t('accepts a delegate like such: function(callback) { ... }.'),
      '#default_value' => $this->options['transout'],
    );
    $form['transin'] = array(
      '#type' => 'textarea',
      '#title' => t('Transition In Function'),
      '#description' => t('accepts a delegate like such: function() { ... }.'),
      '#default_value' => $this->options['transin'],
    );
    $form['pageout'] = array(
      '#type' => 'textarea',
      '#title' => t('Transition Page Out Function'),
      '#description' => t('accepts a delegate like such: function(callback) { ... }.'),
      '#default_value' => $this->options['pageout'],
    );
    $form['pagein'] = array(
      '#type' => 'textarea',
      '#title' => t('Transition Page In Function'),
      '#description' => t('accepts a delegate like such: function() { ... }.'),
      '#default_value' => $this->options['pagein'],
    );
  }
}
